FROM registry.gitlab.com/javacraft/containers/ubuntu:20.04-1

RUN cd /tmp && \
    curl -L https://download.java.net/java/GA/jdk17.0.1/2a2082e5a09d4267845be086888add4f/12/GPL/openjdk-17.0.1_linux-x64_bin.tar.gz -o openjdk.tar.gz && \
    mkdir -p /usr/lib/java && \
    tar -xvf openjdk.tar.gz -C /usr/lib/java && \
    ln -s /usr/lib/jvm/jdk-17 /usr/lib/java/jdk-17.0.1 && \
    update-alternatives --install /usr/bin/java java /usr/lib/java/jdk-17.0.1/bin/java 1171

ENV JAVA_HOME /usr/lib/java/jdk-17.0.1
