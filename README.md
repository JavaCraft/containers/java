# Ubuntu base container

## About

Base container for all JavaCraft containers, based on the Ubuntu 20.04 and Java 17.0.1. 

## Changelog

### 17.0.1-1
- Installed `java` 1.17.1.
